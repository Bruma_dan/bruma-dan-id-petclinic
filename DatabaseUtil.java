package util;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Appointment;
import model.Doctor;


public class DatabaseUtil {

	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;



	// Connecting to the database
	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("TemaBrumaDan");
		entityManager = entityManagerFactory.createEntityManager();
	}

	// adding an animal to the database
	public void addAnimal(int idanimal, String animalname) {
		Animal animal = new Animal();
		animal.setIdanimal(idanimal);
		animal.setAnimalname(animalname);
		entityManager.getTransaction().begin();
		entityManager.persist(animal);
		entityManager.getTransaction().commit();
	}

	//displaying all animals inside the database
	public List<Animal> getAllAnimals() {
		List<Animal> results = entityManager.createNativeQuery("SELECT * FROM Animal", Animal.class).getResultList();
		return results;
	}



	//Adding a doctor to the database
	public void addDr(int iddoctor, String doctorname) {
		Doctor doctor = new Doctor();
		doctor.setIddoctor(iddoctor);
		doctor.setDoctorname(doctorname);
		entityManager.getTransaction().begin();
		entityManager.persist(doctor);
		entityManager.getTransaction().commit();
	}

	//displaying all doctors inside the database
	public List<Doctor> getAllDrs() {
		List<Doctor> results = entityManager.createNativeQuery("SELECT * FROM doctor", Doctor.class).getResultList();
		return results;
	}

	// This method is going to display all employees inside the database
		public List<Appointment> getAllAppointments() {
			List<Appointment> results = entityManager.createNativeQuery("SELECT * FROM appointment", Appointment.class).getResultList();
			return results;
		}
	//adding appointments to the database
	public void addAppointment() throws ParseException {
		Appointment appointment = new Appointment();
		System.out.println("Add appointment id:");
		Scanner read = new Scanner(System.in);
		int id = read.nextInt();
		read.nextLine();
	    System.out.println("Add appointment doctor");
	    String doc = read.nextLine();
	    System.out.println("Add appointment animal");
	    String anim = read.nextLine();
	    appointment.setIdappointment(id);
	    appointment.setDoctor(getDoc(doc));
	    appointment.setAnimal(getAnim(anim));
	    if(appointment.getAnimal() == null) {
	    	System.out.println("Animal does not exist in database. Please make a request for it to be added.");
	    	return;
	    }else if(appointment.getDoctor() == null) {
	    	System.out.println("doctor does not exist in database.");
	    	return;
	    }
		entityManager.getTransaction().begin();
		entityManager.persist(appointment);
		entityManager.getTransaction().commit();
	}
	public Doctor getDoc(String name) {
		List<Doctor> results = entityManager.createNativeQuery("SELECT * FROM doctor WHERE name='" + name + "'", Doctor.class).getResultList();
		int total = 0;
		for (Doctor doctor : results) {
			total++;
		}
		if(total == 0) {
			return null;
		}
		return results.get(0);

	}
	public Animal getAnim(String name) {
		List<Animal> result = entityManager.createNativeQuery("SELECT * FROM Animal WHERE name='" + name + "'", Animal.class).getResultList();
		int total = 0;
		for (Animal animal : result) {
			total++;
		}
		if(total == 0) {
			return null;
		}
		return result.get(0);

	}
	
	//Closing the connection to the database
	public void closeConnection() {
		entityManager.close();
		entityManagerFactory.close();
	}

}