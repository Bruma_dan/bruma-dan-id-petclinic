package main;

import java.io.IOException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import util.DatabaseUtil;

public class Main extends Application {
	private static Stage primaryStage;
	private static BorderPane mainLayout;	
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Main.primaryStage = primaryStage;
		Main.primaryStage.setTitle("Animal Clinic App");
		showMainView();
	}
	public void showMainView() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/views/MainView.fxml"));
		mainLayout = loader.load();
		Scene scene = new Scene(mainLayout);
		primaryStage.setScene(scene);
		primaryStage.show();

	}
	public static void showMainItems() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/views/MainView.fxml"));
		BorderPane MainItems = loader.load();
		mainLayout.setCenter(MainItems);
	}

	public static void showPatients() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/views/PatientsView.fxml"));
		BorderPane patientsView = loader.load();
		mainLayout.setCenter(patientsView);
	}
	public static void showDoctors() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/views/DoctorsView.fxml"));
		BorderPane doctorsView = loader.load();
		mainLayout.setCenter(doctorsView);
	}

	public static void showAppointments() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/views/AppointmentsView.fxml"));
		BorderPane appointmentsView = loader.load();
		mainLayout.setCenter(appointmentsView);
	}
	
	public static void addDoctors() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/views/AddNewDoctor.fxml"));
		BorderPane addNewDoctor = loader.load();
		
		Stage addDoctorStage = new Stage();
		addDoctorStage.setTitle("Add New Doctor");
		addDoctorStage.initModality(Modality.WINDOW_MODAL);
		addDoctorStage.initOwner(primaryStage);
		Scene scene = new Scene(addNewDoctor);
		addDoctorStage.setScene(scene);
		addDoctorStage.showAndWait();
		
	}
	public static void addPatients() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/views/AddNewPatient.fxml"));
		BorderPane addNewPatient = loader.load();
		
		Stage addPatientStage = new Stage();
		addPatientStage.setTitle("Add New Patient");
		addPatientStage.initModality(Modality.WINDOW_MODAL);
		addPatientStage.initOwner(primaryStage);
		Scene scene = new Scene(addNewPatient);
		addPatientStage.setScene(scene);
		addPatientStage.showAndWait();
		
	}
	public static void addAppointments() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/views/AddNewAppointment.fxml"));
		BorderPane addNewAppointment = loader.load();
		
		Stage addAppointmentStage = new Stage();
		addAppointmentStage.setTitle("Add New Appointment");
		addAppointmentStage.initModality(Modality.WINDOW_MODAL);
		addAppointmentStage.initOwner(primaryStage);
		Scene scene = new Scene(addNewAppointment);
		addAppointmentStage.setScene(scene);
		addAppointmentStage.showAndWait();
		
	}

	public static void main(String[] args) {
		launch(args);
	}
		
}
